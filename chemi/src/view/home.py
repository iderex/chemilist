from chemi import app
from chemi.src.info import getInfo, getCompound
from flask import flash, render_template, session, request


@app.route("/", methods=["POST", "GET"])
def home():

    if request.method == "POST":
        posts = request.form
        for post in posts.items():
            compound = post[1].lower()

        try:
            initInfo = getCompound(compound)
            info = getInfo(initInfo[4])  # initInfo[4] = CID

            error = False

        except TypeError as e:
            print(e)
            error = True

        return render_template("home.html",
                               imageSource=initInfo[0],
                               compound=compound,
                               pageURL=initInfo[1],
                               IUPAC=initInfo[2],
                               formula=info[0],
                               weight=info[1],
                               preferred=initInfo[3],
                               error=error,
                               isomericSmiles=info[4],
                               hydrogenBondAcceptorCount=info[3],
                               IUPACInChI=info[2],
                               MonoIsotopicWeight=info[6],
                               IsotopeAtomCount=info[7],
                               RotatableBondCount=info[8],
                               StructureComplexity=info[9],
                               HydrogenBondDonorCount=info[5],
                               NonHydrogenAtomCount=info[10],
                               UndefinedAtomStereoCount=info[11],
                               UndefinedBondStereoCount=info[12],
                               TPSA=info[13],
                               TotalFormalCharge=info[14],
                               CanonicalSMILES=info[15],
                               CompoundIdentifier=info[16],
                               DefinedBondStereoCount=info[17],
                               ExactMass=info[18],
                               CovalentUnitCount=info[19],
                               DefinedAtomStereoCount=info[20],
                               XLogP3=info[21]
                               )

    return render_template("home.html")
